# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class WbcsspiderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    # 房屋链接获取
    house_info=scrapy.Field()
    # 房屋标题获取
    house_title=scrapy.Field()
    #上架时间
    house_time=scrapy.Field()
    # 房屋售价获取
    house_sum=scrapy.Field()
    # 房屋售价单位
    house_sum_unit=scrapy.Field()
    # 房屋每平方米售价
    house_num_chushou=scrapy.Field()
    # 房屋预期收益(可能为空)
    house_basic_yuqi=scrapy.Field()
    # 物业,电费,水费(二次处理可能为空)
    house_wuye=scrapy.Field()
    house_wuye_elec=scrapy.Field()
    house_wuye_water=scrapy.Field()

    # 房屋细节

    #房屋面积
    house_area=scrapy.Field()
    # 房屋位置类型
    house_type=scrapy.Field()
    # 位置类型备注
    house_type_remark=scrapy.Field()
    # 面宽
    house_width=scrapy.Field()
    # 经营状态
    house_type_run=scrapy.Field()
    # 进深
    house_length=scrapy.Field()
    # 经营行业
    house_industry=scrapy.Field()
    # 层高
    house_high=scrapy.Field()
    # 楼层
    house_floor=scrapy.Field()

    # 详细地址

    # 区域
    house_address_area=scrapy.Field()
    # 街道
    house_address_street=scrapy.Field()
    # 具体地址
    house_address_village=scrapy.Field()
    # 联系人
    house_linkman=scrapy.Field()
    # 联系电话
    house_tel=scrapy.Field()
    # 具体描述
    remarks=scrapy.Field()



    pass
